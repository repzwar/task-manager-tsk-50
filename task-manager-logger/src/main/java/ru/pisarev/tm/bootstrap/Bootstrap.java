package ru.pisarev.tm.bootstrap;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IReceiverService;
import ru.pisarev.tm.listener.LogMessageListener;
import ru.pisarev.tm.service.ReceiverService;

import static ru.pisarev.tm.constant.ActiveMQConst.URL;

public final class Bootstrap {

    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LogMessageListener());
    }

}
