package ru.pisarev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.IRecordService;
import ru.pisarev.tm.api.service.IConnectionService;
import ru.pisarev.tm.dto.AbstractRecord;

public abstract class AbstractRecordService<E extends AbstractRecord> implements IRecordService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractRecordService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
