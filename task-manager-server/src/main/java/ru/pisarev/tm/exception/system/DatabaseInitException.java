package ru.pisarev.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class DatabaseInitException extends AbstractException {

    @NotNull
    public DatabaseInitException() {
        super("Error. Database initialization failed.");
    }

}
